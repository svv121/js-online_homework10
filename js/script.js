"use strict";
/*
*/
const tabsWithTexts = document.querySelector(".centered-content");
const tabTitles = document.querySelectorAll(".tabs-title");
const tabTexts = document.querySelectorAll(".tab-text");
tabsWithTexts.addEventListener("click", (event) => {
    const id = event.target.dataset.id;
    if (id) {
        tabTitles.forEach(title => {
            title.classList.remove("active");
        });
        event.target.classList.add("active");
        tabTexts.forEach(text => {
            text.classList.remove("active");
        });
        const element = document.getElementById(id);
        element.classList.add("active");
    }
})